from flask import Flask, render_template, make_response, redirect
import os
# import json to load JSON data to a python dictionary
import json
  
# urllib.request to make a request to api
import urllib.request

from arduino import Arduino
from database import Database

print(os.path)
app = Flask(__name__)
ard = Arduino("/dev/ttyACM0")
db = Database("mongodb://localhost:27017/")
db.add_user({"plantes": ["Persil", "Basilique", "Tomates", "Concombres"]})

import datetime
from random import randint
import time

for i in range(3):
    date_time = datetime.datetime.now()

    arduino_state = {"pompes": (randint(0, 1),
                                randint(0, 1),
                                randint(0, 1),
                                randint(0, 1)),
                     "senseurs": (randint(500, 550),
                                  randint(500, 550),
                                  randint(500, 550),
                                  randint(500, 550),
                                  randint(150, 175),
                                  randint(5, 30))}

    pompe1 = arduino_state["pompes"][0]
    pompe2 = arduino_state["pompes"][1]
    pompe3 = arduino_state["pompes"][2]
    pompe4 = arduino_state["pompes"][3]
    humidite1 = arduino_state["senseurs"][0]
    humidite2 = arduino_state["senseurs"][1]
    humidite3 = arduino_state["senseurs"][2]
    humidite4 = arduino_state["senseurs"][3]
    temperature = arduino_state["senseurs"][4]
    reservoir = arduino_state["senseurs"][5]

    document = {"date": date_time,
                "temperature": temperature,
                "reservoir": reservoir,
                "humidite1": humidite1,
                "humidite2": humidite2,
                "humidite3": humidite3,
                "humidite4": humidite4,
                "pompe1": pompe1,
                "pompe2": pompe2,
                "pompe3": pompe3,
                "pompe4": pompe4}

    db.add_data(document)

    time.sleep(1.5)
print(db.read_data({"pompe4": 0}, 4))


@app.route("/")
def index():
    ard_data = ard.state
    humidite1 =ard_data["sensors"][0]
    humidite2 =ard_data["sensors"][1]
    humidite3 =ard_data["sensors"][2]
    humidite4 =ard_data["sensors"][3]

    return render_template("index.html",humidite1 = humidite1, humidite2 = humidite2, humidite3 = humidite3, humidite4 = humidite4)

@app.route("/plante/<id>")
def plante(id):
    try:
        ard_data = ard.state
        temperature = ard_data["sensors"][4]
        humidite = ard_data["sensors"][int(id)-1]
        etat_pompe = ard_data["pompes"][int(id)-1]
        print(temperature, humidite)

    #foncitonne
    except Exception as e:
        print("erreur", e)
        temperature = 25
        humidite = 50
        print(temperature, humidite)

    try:
        user_data = db.read_user({})
        nom = user_data["plantes"][int(id)-1]

    except:
        nom = "Plante 1"


    return render_template("plante.html", nom = nom, plant_id = id, humidite = humidite, temperature = temperature, etat_pompe= etat_pompe)

@app.route("/historique")
def historique():
    return render_template("historique.html")


@app.route("/arduino/state")
def arduino_state():
    return ard.state

@app.route("/arduino/cmd/<id>")
def arduino_switch_pump(id):
    id = int(id)
    try:
        ard.set_cmd(id)
        print(id)
        if id < 5:
            time.sleep(1.5)
            return redirect(f"http://127.0.0.1:5000/plante/{id}")
        else:
            return redirect("http://127.0.0.1:5000")
    except:
        return make_response("Didn't work correctly!", 200)

@app.route("/arduino/state")
def arduino(method):
    return ard.state

@app.route("/database/<document>")
def database(document:dict):
    db.add_data(document)
    return document


if __name__ == "__main__":
    ard.run()
    app.run()
    print("test")