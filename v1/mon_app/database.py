#!/usr/bin/env python
# coding: utf-8

import pymongo

class Database:
    def __init__(self, uri):
        self.client = pymongo.MongoClient(uri)
        self.database = self.client['serre-serre_database']
        self.arduino_data = self.database['arduino_data']
        self.user_data = self.database['user_data']
        print(f"Client connected( {uri} )")

    def add_data(self, document: dict):
        """
        modele de données: {'_id': ObjectId('6202cf04d8458e40321e957a'),
                            'date': datetime.datetime(2022, 2, 8, 15, 13, 56, 597000),
                            'temperature': 200,
                            'reservoir': 0,
                            'humidite1': 500,
                            'humidite2': 500,
                            'humidite3': 500,
                            'humidite4': 500,
                            'pompe1': 0,
                            'pompe2': 0,
                            'pompe3': 0,
                            'pompe4': 0}
        """
        self.arduino_data.insert_one(document)
        print(f"Add {document}")


    def read_data(self, query: dict, limit: int = 1):
        """ Retourne un dictionnaire ou une liste de dictionnaires selon le nombre de documents trouvés."""
        response = self.arduino_data.find(query).limit(limit)
        results = []
        for i in response:
            results.append(i)

        print(f"Found {len(results)} document using filter: {query}" if len(results) <= 1 \
                  else f"Found {len(results)} documents using filter: {query}")
        return results[0] if len(results) == 1 else results

    def wipe_data(self):
        """Effacer l'historique des données d'états du Arduino"""
        self.arduino_data.drop()

    def add_user(self, document: dict):
        """
        Modele de données: {"username": username,
                            "password": Hashed.password,
                            "styles": "styles_1.css",
                            "plantes": ["Plante 1","Plante 2","Plante 3","Plante 4"],
                            "auto_humidité": {"minimum": [15,15,55,55], "duree": [15,15,55,55]}
                            "auto_journalier": {"début": [[7,15],[7,15],[19,0],[22,0]], "duree": [200,200,55,55]}
                            "auto_suggestion": {"parametre1: [0,1,2,3], "parametre2: [a,b,c,d]}
                            }
        """
        self.user_data.insert_one(document)
        print(f"Add {document}")

    def read_user(self, query: dict, limit: int = 1):
        """Retourne un dictionnaire ou une liste de dictionnaires"""
        response = self.user_data.find(query).limit(limit)
        results = []
        for i in response:
            results.append(i)
            print(i)
        return results[0] if len(results) == 1 else results

    def update_user(self, query: dict, new_data: dict ):
        self.user_data.update_one(query, new_data)
        print("Updated ")

    def delete_user(self, query):
        self.user_data.delete_one(query)




if __name__ == "__main__":


    URI = "mongodb://localhost:27017/"
    DB_NAME = "serre_database"
    COL_NAME = "plantes"


    import datetime
    date_time = datetime.datetime.now()
    date, time = str(date_time).split()
    annee, mois, jour = date.split("-")
    heure, minute = time.split(":")[:2]
    print(str(date_time))
    print(annee, mois, jour, heure, minute)

    arduino_state = {"pompes": (0, 0, 0, 0), "senseurs": (500, 500, 500, 500, 200, 0)}

    pompe1 = arduino_state["pompes"][0]
    pompe2 = arduino_state["pompes"][1]
    pompe3 = arduino_state["pompes"][2]
    pompe4 = arduino_state["pompes"][3]
    humidite1 = arduino_state["senseurs"][0]
    humidite2 = arduino_state["senseurs"][1]
    humidite3 = arduino_state["senseurs"][2]
    humidite4 = arduino_state["senseurs"][3]
    temperature = arduino_state["senseurs"][4]
    reservoir = arduino_state["senseurs"][5]

    document = {"date": date_time,
                # "annee": annee,
                # "mois": mois,
                # "jour": jour,
                # "heure": heure,
                # "minute": minute,
                "temperature": temperature,
                "reservoir": reservoir,
                "humidite1": humidite1,
                "humidite2": humidite2,
                "humidite3": humidite3,
                "humidite4": humidite4,
                "pompe1": pompe1,
                "pompe2": pompe2,
                "pompe3": pompe3,
                "pompe4": pompe4,
                }

    db = Database(URI)
    db.wipe_data()


    db.add_data(document)
    print(db.read_data({"pompe4": 0}, 4))

    import time
    from random import randint
    for i in range(3):
        date_time = datetime.datetime.now()
        date, temps = str(date_time).split()
        annee, mois, jour = date.split("-")
        heure, minute = temps.split(":")[:2]
        print(str(date_time))
        print(annee, mois, jour, heure, minute)

        arduino_state = {"pompes": (randint(0, 1),
                                    randint(0, 1),
                                    randint(0, 1),
                                    randint(0, 1)),
                         "senseurs": (randint(500, 550),
                                      randint(500, 550),
                                      randint(500, 550),
                                      randint(500, 550),
                                      randint(150, 175),
                                      randint(5,30))}

        pompe1 = arduino_state["pompes"][0]
        pompe2 = arduino_state["pompes"][1]
        pompe3 = arduino_state["pompes"][2]
        pompe4 = arduino_state["pompes"][3]
        humidite1 = arduino_state["senseurs"][0]
        humidite2 = arduino_state["senseurs"][1]
        humidite3 = arduino_state["senseurs"][2]
        humidite4 = arduino_state["senseurs"][3]
        temperature = arduino_state["senseurs"][4]
        reservoir = arduino_state["senseurs"][5]

        document = {"date": date_time,
                    # "annee": annee,
                    # "mois": mois,
                    # "jour": jour,
                    # "heure": heure,
                    # "minute": minute,
                    "temperature": temperature,
                    "reservoir": reservoir,
                    "humidite1": humidite1,
                    "humidite2": humidite2,
                    "humidite3": humidite3,
                    "humidite4": humidite4,
                    "pompe1": pompe1,
                    "pompe2": pompe2,
                    "pompe3": pompe3,
                    "pompe4": pompe4}

        db.add_data(document)
        time.sleep(1.5)

    # Query : get a
    print(db.read_data({"pompe4": 0}, 4))