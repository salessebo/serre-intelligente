int IN1 = 2; // Pompe1
int IN2 = 3; // Pompe2
int IN3 = 4; // Pompe3
int IN4 = 5; // Pompe4
int INs[] = {IN1,IN2,IN3,IN4};

int pin_ = 0; //
int Pin1 = A0; // MoistureSensor1
int Pin2 = A1; // MoistureSensor2
int Pin3 = A2; // MoistureSensor3
int Pin4 = A3; // MoistureSensor4
int Pin5 = A4; // TemperatureSensor
int Pin6 = A5; // empty

int value1 = 0;
int value2 = 0;
int value3 = 0;
int value4 = 0;
int value5 = 0;
int value6 = 0;

int saisie = 0;


void setup() {
  Serial.begin(9600);
  Serial.println("Setup start");
  pinMode(Pin1, INPUT);
  pinMode(Pin2, INPUT);
  pinMode(Pin3, INPUT);
  pinMode(Pin4, INPUT);
  pinMode(Pin5, INPUT);
  pinMode(Pin6, INPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, HIGH);
  delay(500);
  
//  test_relay();
Serial.println("Setup end");
}

void loop() {
  Serial.println("Loop start");

  while (Serial.available() == 0) {
    publish_states();
    delay(100);
  }

  saisie = Serial.parseInt();

  Serial.print("Saisie: ");
  Serial.println(saisie);
  switch (saisie) {
    case 1:
      switchOnOff(IN1);
      break;
    case 2:
      switchOnOff(IN2);
      break;
    case 3:
      switchOnOff(IN3);
      break;
    case 4:
      switchOnOff(IN4);
      break;
    case 5:
      switchOff();
      break;
    default:
      // statements
      break;
  }
  Serial.println("Loop end");
}

void switchOnOff(int pin_) {
  if (digitalRead(pin_)) {
    digitalWrite(pin_, LOW);
  }
  else {
    digitalWrite(pin_, HIGH);
  }
}

void switchOff () {
  for (int i=0; i<sizeof INs/sizeof INs[0]; i++) {
    digitalWrite(INs[i], HIGH);
    }
}

void publish_states() {
  //  Lecture des senseurs
  value1 = analogRead(Pin1);
  value2 = analogRead(Pin2);
  value3 = analogRead(Pin3);
  value4 = analogRead(Pin4);
  value5 = analogRead(Pin5);
  value6 = analogRead(Pin6);
  // Imprimer les valeurs des pompes (0 = OFF, 1 = ON)
  Serial.print("{'pompes':(");
  Serial.print(!digitalRead(IN1));
  Serial.print(", ");
  Serial.print(!digitalRead(IN2));
  Serial.print(", ");
  Serial.print(!digitalRead(IN3));
  Serial.print(", ");
  Serial.print(!digitalRead(IN4));
  Serial.print("), 'sensors': (");
  // Imprimer les valeurs des senseurs
  Serial.print(value1);
  Serial.print(", ");
  Serial.print(value2);
  Serial.print(", ");
  Serial.print(value3);
  Serial.print(", ");
  Serial.print(value4);
  Serial.print(", ");
  Serial.print(value5);
  Serial.print(", ");
  Serial.print(value6);
  Serial.println(")}");
}



void test_relay() {
  Serial.println("TEST POMPES START");
  digitalWrite(IN1, LOW);
  delay(300);
  digitalWrite(IN2, LOW);
  delay(300);
  digitalWrite(IN3, LOW);
  delay(300);
  digitalWrite(IN4, LOW);
  delay(600);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, HIGH);
  Serial.println("TEST POMPES FIN");
}
